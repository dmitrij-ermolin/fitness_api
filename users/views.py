from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins
from .serializers import ActivitySerializer, UserSerializer
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status
from .models import Activity, User
import datetime

class UserViewSet(mixins.RetrieveModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):

    def retrieve(self, request, pk=None, *args, **kwargs):
        queryset = User.objects.all()
        activity = get_object_or_404(queryset, pk=pk)
        serializer_class = UserSerializer(activity)
        return Response(serializer_class.data.get('activity'))

    def create(self, request, *args, **kwargs):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, email=request.data['email'])
        if user is None:
            res = {'error': 'please provide a valid email'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        try:
            activity_data = request.data['activity']
        except KeyError:
            res = {'error': 'please provide a valid activity'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        activity_data['activity_id'] = user.id
        serializer_class = ActivitySerializer(data=activity_data)
        if serializer_class.is_valid():
            Activity.objects.create(name=activity_data['name'], begin_data=activity_data['begin_data'],
                                                   end_data=activity_data['end_data'],
                                                   amount_of_kkal=activity_data['amount_of_kkal'],
                                                   amount_of_kilometers=activity_data['amount_of_kilometers'],
                                                   activity_id=user.id)

            return Response(serializer_class.data)
        res = {'error': 'please provide a valid activity'}
        return Response(res, status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['post'], url_path='by_hour')
    def activity_by_hour(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        if user is None:
            res = {'error': 'please provide a valid email'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        try:
            start_time = datetime.datetime.strptime(request.data.get('start'), '%Y-%m-%dT%H:%M:%S.%fZ')
        except KeyError:
            res = {'error': 'please provide a valid begin or end'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        start_hour = datetime.datetime(start_time.year, start_time.month, start_time.day, start_time.hour)
        end_hour = datetime.datetime(start_time.year, start_time.month, start_time.day, start_time.hour+1)

        activity_by_hour = Activity.objects.filter(begin_data__range=(start_hour, end_hour),
                                                   activity_id=user.id).order_by('begin_data')
        if not activity_by_hour:
            res = {'error': 'not found'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        serializer_class = ActivitySerializer(activity_by_hour, many=True)
        return Response(serializer_class.data)

    @action(detail=True, methods=['post'], url_path='by_day')
    def activity_by_day(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        if user is None:
            res = {'error': 'please provide a valid email'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        try:
            start_time = datetime.datetime.strptime(request.data.get('start'), '%Y-%m-%dT%H:%M:%S.%fZ')
        except KeyError:
            res = {'error': 'please provide a valid begin or end'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        start_day = datetime.date(start_time.year, start_time.month, start_time.day)
        end_day = datetime.date(start_time.year, start_time.month, start_time.day + 1)
        activity_by_day = Activity.objects.filter(begin_data__range=(start_day, end_day)).order_by('begin_data')
        if not activity_by_day:
            res = {'error': 'not found'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
        serializer_class = ActivitySerializer(activity_by_day, many=True)
        return Response(serializer_class.data)









